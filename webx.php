﻿<html>

<head> 
	<title> PokemonGO Calculator </title>
	<link rel="shortcut icon" href="/Pokeball_icon.ico" />
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
	<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
	
        <!-- Bootstrap CSS and bootstrap datepicker CSS used for styling the demo pages-->
        <link rel="stylesheet" href="css/datepicker.css">
        <link rel="stylesheet" href="css/bootstrap.css">
		  <!-- Load jQuery and bootstrap datepicker scripts -->
        <script src="js/jquery-1.9.1.min.js"></script>
        <script src="js/bootstrap-datepicker.js"></script>
        <script type="text/javascript">
			 $(document).ready(function () {
                
                $('#datePick').datepicker({
                    format: "dd-mm-yyyy"
                });  
            
            });
		</script>
		
		<script type="text/javascript">
		
		function levelCheck(n){
			numRegex=/^\d{0,2}$/

				if ( n.value < 1 || n.value > 40 || !n.value.match(numRegex)){
					alert("You can reach maximum level 40!");
					return false;
					}
					return true;
			}
		</script>
		
		<script type="text/javascript">
			
			function submitCheck(s){
				numRegex=/^\d{0,2}$/
				info="";
				
				if (s.lvlNow.value < 1 || s.lvlNow.value > 40 || !s.lvlNow.value.match(numRegex) || s.lvlThen.value < 1 || s.lvlThen.value > 40 || !s.lvlThen.value.match(numRegex))
					info = info+"Please insert correct level\n";

				if(info){
					alert(info);
					return false;
				}
				
			}	
		</script>
		
		
</head>

<body background="http://wallpapercave.com/wp/Qf9SNmS.png">

	<br><br><br>
<div class="container">
	<div class="jumbotron">
	
	
    <h2> PokemonGO Experience Calculator</h2>      
    <p>Here you can calculate what you have to do to earn enough experience to gain a higher level in PokemonGO</p><br>
	
	<form class="form-inline" role="form" action="calculate.php" method="POST" onSubmit="return submitCheck(this)">
		<div class="form-group">
	 
	 <div class="row">
		<div class="col-sm-6"><b>Your actual level</b></div>
		<div class="col-sm-4"><input class="form-control" id="lvlNow" type="text" name="lvlNow" onBlur="levelCheck(lvlNow)"></div>
		<br><br>
	</div>
		
	<div class="row">
		<div class="col-sm-6"><b>Expected level</b></div>
		<div class="col-sm-4"><input class="form-control" id="lvlThen" type="text"  name="lvlThen" onBlur="levelCheck(lvlThen)"></div>
		<br><br>
	</div>
			
	<div class="row">
		<div class="col-sm-6"><b>Your experience</b></div>
		<div class="col-sm-4"><input class="form-control" id="exp" type="text"  name="exp"></div>
		<br><br>
	</div>
		  
	<div class="row">
		<div class="col-sm-6"><b>Starting date</b></div>
		<div class="col-sm-4"><input class="form-control" type="text" placeholder="click to show"  id="datePick" name="startDate"></div>
		<br><br>
	</div>
		  
	<input type="submit" onClick="submitCheck(this)" class="btn btn-primary">
	
		</div>
	</form>
	
		   
		   
<footer><center>Alpha version.<br> New features in the near future.<br>

<?php

if(!isset($_COOKIE['cookie'])){

$expire=time()+15;
setcookie('cookie','visited',$expire);
$f=fopen('visits.txt','r');
$visitors=intval(fgets($f,100));
$visitors=$visitors+1;
$f=fopen('visits.txt','w');
fwrite($f,$visitors);

}else{

$f=fopen('visits.txt','r');
$visitors=fgets($f,100);
echo "You are $visitors visitor";
fclose($f);
}

?>

</footer>

  </div>
</div>

</body>
</html>